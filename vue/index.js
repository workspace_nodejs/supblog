new Vue({
  el: "#articlesContainer",

  data: {
    articles: [],
    comments: [],
    likes: []
  },

  mounted() {
    fetch("http://localhost:3000/articles/published")
      .then(response => response.json())
      .then(result => {
        this.articles = result;
      });

    fetch("http://localhost:3000/comments")
      .then(response => response.json())
      .then(result => {
        this.comments = result;
      });

    fetch("http://localhost:3000/likes")
      .then(response => response.json())
      .then(result => {
        this.likes = result;
      });
  }
});
