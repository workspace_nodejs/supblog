new Vue({
  el: "#articlesContainer",

  data: {
    articles: []
  },

  mounted() {
    var currentUrl = window.location.pathname;
    var url = "http://localhost:3000/article/" + currentUrl.substring(21);

    fetch(url)
      .then(response => response.json())
      .then(result => {
        this.articles = result;
      });
  }
});
