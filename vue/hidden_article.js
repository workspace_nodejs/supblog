new Vue({
  el: "#articlesContainer",

  data: {
    articles: [],
    comments: [],
    likes: []
  },

  mounted() {
    fetch("http://localhost:3000/articles/hidden")
      .then(response => response.json())
      .then(result => {
        this.articles = result;
      });
  }
});
