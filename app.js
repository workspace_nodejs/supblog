const express = require("express");
const app = express();
const db = require("./controller/db");
const http = require("http").Server(app);
const path = require("path");
const url = require("url");
const route = express.Router();
const bodyParser = require("body-parser");
const session = require("express-session");
const cookieParser = require("cookie-parser");

const port = 3000;
var page_count = 0;

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(
  session({
    secret: "This is a secret",
    resave: true,
    saveUninitialized: true
  })
);

const server = app.listen(port, function() {
  console.log("[SERVER] - Server working on port : " + port);
});

const io = require("socket.io")(server);

//=========================== ROUTES ==============================

route.all("*", function(req, res, next) {
  page_count++;
  //console.log("Pages : " + page_count);
  next();
});

route.all(["/", "/index"], function(req, res) {
  res.sendFile("/views/index.html", { root: __dirname });
});

route.all("/old", function(req, res) {
  res.sendFile("/views/indexOldOld.html", { root: __dirname });
});

//=========================== REGISTER ==============================

route.get("/register", function(req, res) {
  res.sendFile("/views/register.html", { root: __dirname });
});

route.post("/newUser", function(req, res) {
  promise = db.isUnique(req.body.username);
  promise.then(function(nbUser) {
    if (nbUser == 0) {
      db.newUser({
        username: req.body.username,
        password: req.body.password,
        mail: req.body.mail,
        isadmin: false
      });
      console.log("[REGISTER] - Register ok");
      res
        .status(200)
        .send(
          "A mail has been send to your mail adress to validate your account !"
        );
    } else {
      console.log("[REGISTER] - Username taken");
      res.status(500).send("The username is taken, please choose another");
    }
  });
});

route.all("/activate/:id", function(req, res) {
  var id = req.params.id;
  db.activateUser(id);
  res.sendFile("/views/index.html", { root: __dirname });
});

//=========================== LOGIN / LOGOUT ==============================

route.get("/login", function(req, res) {
  res.sendFile("./views/login.html", { root: __dirname });
});

route.post("/login", function(req, res) {
  promise = db.exists(req.body.username, req.body.password);
  promise.then(function(userExist) {
    if (userExist == 1) {
      promise = db.getUserByUsername(req.body.username);
      promise
        .then(function(userDetails) {
          try {
            userDetails = JSON.parse(userDetails);
          } catch (err) {
            console.error(err);
          }
          if (userDetails[0].active) {
            req.session.id = userDetails[0]._id;
            req.session.username = userDetails[0].username;
            req.session.admin = userDetails[0].admin;
            if (req.session.admin) {
              console.log("[LOGIN] - Logged as admin");
              res.sendFile("/views/admin.html", { root: __dirname });
            } else {
              console.log("[LOGIN] - Logged as user");
              res.sendFile("/views/index.html", { root: __dirname });
            }
          } else {
            console.log("[LOGIN] - Account not activated");
            res
              .status(500)
              .send(
                "You need to activate your account ! Please check your mail"
              );
          }
        })
        .catch(err => {
          res.status(500).json({ error: err.message });
        });
    } else {
      console.log("[LOGIN] - Bad username / password");
      res.status(500).send("Bad username / password!");
    }
  });
});

route.all("/logout", function(req, res) {
  req.session.destroy(function() {
    console.log("[LOGOUT] - User session terminated");
  });
  res.sendFile("/views/index.html", { root: __dirname });
});

route.all("/publish/:id", function(req, res) {
  var id = req.params.id;
  db.publishArticle(id);
  res.sendFile("/views/hiddenarticles.html", { root: __dirname });
});

//=========================== COMMENT ==============================

route.post("/newComment", function(req, res) {
  db.newComment(req.session.username, req.body.content, req.body.button);
  res.sendFile("/views/index.html", { root: __dirname });
});

//=========================== LIKE ==============================

route.all("/like/:id", function(req, res) {
  var id = req.params.id;
  db.newLike(req.session.username, id);
  res.sendFile("/views/index.html", { root: __dirname });
});

//=========================== ADMIN ==============================

route.all("/admin/*", function(req, res, next) {
  if (req.session.admin) {
    next();
  } else {
    console.log("[ADMIN] - A user try to go to a admin reserved page");
    res.sendFile("/views/index.html", { root: __dirname });
  }
});

route.get("/admin/index", function(req, res) {
  res.sendFile("/views/admin.html", { root: __dirname });
});

route.get("/admin/newarticle", function(req, res) {
  res.sendFile("/views/newarticle.html", { root: __dirname });
});

route.post("/admin/newArticle", function(req, res) {
  if (req.body.action === "save") {
    db.newArticle({
      title: req.body.articleTitle,
      content: req.body.content,
      date: new Date(),
      isPublished: false
    });
    res.sendFile("/views/admin.html", { root: __dirname });
  } else {
    db.newArticle({
      title: req.body.articleTitle,
      content: req.body.content,
      date: new Date(),
      isPublished: true
    });
    res.sendFile("/views/admin.html", { root: __dirname });
  }
});

route.get("/admin/updatearticle/:id", function(req, res) {
  res.sendFile("/views/updatearticle.html", { root: __dirname });
});

route.post("/admin/updatearticle/:id", function(req, res) {
  var id = req.params.id;
  if (req.body.action === "save") {
    db.updateArticle({
      id: id,
      title: req.body.articleTitle,
      content: req.body.content,
      date: new Date(),
      isPublished: false
    });
    res.sendFile("/views/admin.html", { root: __dirname });
  } else {
    db.newArticle({
      id: id,
      title: req.body.articleTitle,
      content: req.body.content,
      date: new Date(),
      isPublished: true
    });
    res.sendFile("/views/admin.html", { root: __dirname });
  }
});

route.all("/admin/hiddenarticle", function(req, res) {
  res.sendFile("/views/hiddenarticles.html", { root: __dirname });
});

//=========================== API ==============================

route.all("/articles/:filter", function(req, res) {
  var filter = req.params.filter; //published, hidden, all
  var promise = db.getApiArticles(filter);
  promise
    .then(function(articles) {
      res.send(articles);
    })
    .catch(err => {
      res.status(500).json({ error: err.message });
    });
});

route.all("/article/:id", function(req, res) {
  var id = req.params.id;
  var promise = db.getApiArticle(id);
  promise
    .then(function(articles) {
      res.send(articles);
    })
    .catch(err => {
      res.status(500).json({ error: err.message });
    });
});

route.all("/comments", function(req, res) {
  promise = db.getAllComments();
  promise
    .then(function(comments) {
      res.send(comments);
    })
    .catch(err => {
      res.status(500).json({ error: err.message });
    });
});

route.all("/likes", function(req, res) {
  promise = db.getAllLikes();
  promise
    .then(function(likes) {
      res.send(likes);
    })
    .catch(err => {
      res.status(500).json({ error: err.message });
    });
});

app.use(route);

db.connect();
//db.disconnect();
