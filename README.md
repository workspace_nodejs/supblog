# SupBlog

**1. Context**

Your entreprise needs a blog to communicate about new products. This blog has to manage two different
roles: Administrator & visitor.
The administrator is the one who knows the products. He will create the different articles.
Visitors will see the articles and comment them
Your company specializes in the web, and you must use Node.js with some HTML5 APIs.

![Supblog preview](img/preview.png)

SupBlog is the most powerful blog editing platform
Write articles, let your visitors like, comment, and manage their liked articles, and spread your propaganda :) !

## Download and Installation

- Clone the latest release on Gitlab 'https://gitlab.com/Rodger_Jr/supblog.git'
- Install via npm to load all the necessary modules 'npm install'
- run app via node : 'node app.js'
- by default, SupBlog is set to listen on port 3000. Open your favorite web browser and go to 'localhost:3000'

## Features

When you are not logged in you first land on the index page, where all the articles are listed. you can register through the menu on the left <br />
Once signed, you can now comment and like articles <br />
Admin users can create new articles via New article, publish hidden articles, and edit articles <br />
If an admin comments an article, his username is displayed in red, and everyone shoud behave ! <br />

## Credentials

for test purposes, a regular and an admin user have been setup

- Admin :
    - username : admin
    - password : admin

- User :
    - username : user
    - password : user

you also have access to the MongoDB sample database to check the integrity of the data during the test

- MongoDB :
    - mail : 292545@supinfo.com
    - password : #motdepasse1

## Documentation

The UML chart describing the structure of the database is available under the /doc directory of the project