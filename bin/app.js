const express = require("express");
const app = express();
const db = require("./controller/db");
const http = require("http").Server(app);
const path = require("path");
const url = require("url");
const route = express.Router();
const bodyParser = require("body-parser");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const io = require("socket.io")(http);

const port = 3000;
var page_count = 0;

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({ secret: "Shh, its a secret!" }));

app.listen(port, function() {
  console.log("Le serveur fonctionne sur le port " + port);
});

route.all("*", function(req, res, next) {
  page_count++;
  console.log("Pages : " + page_count);
  next();
});

route.all(["/", "/index"], function(req, res) {
  res.sendFile("/views/index.html", { root: __dirname });
});

route.all("/activate/:id", function(req, res) {
  var id = req.params.id;
  db.activateUser(id);
  res.sendFile("/views/index.html", { root: __dirname });
});

route.get("/register", function(req, res) {
  res.sendFile("/views/register.html", { root: __dirname });
});

route.post("/newUser", async (req, res) => {
  nbUser = await db.isUnique(req.body.username);
  if (nbUser == 0) {
    db.newUser({
      username: req.body.username,
      password: req.body.password,
      mail: req.body.mail,
      isadmin: false
    });
    res.status(200).send("Connected !");
  } else {
    res.status(500).send("The username is taken, please choose another");
  }
});

route.get("/login", function(req, res) {
  res.sendFile("./views/login.html", { root: __dirname });
});

route.post("/login", async (req, res) => {
  userExist = await db.exists(req.body.username, req.body.password);
  if (userExist == 1) {
    userDetails = await db.getUserByUsername(req.body.username);
    try {
      userDetails = JSON.parse(userDetails);
    } catch (e) {
      console.log(userDetails);
      console.error(e);
    }
    if (userDetails[0].active) {
      req.session.id = userDetails[0]._id;
      req.session.username = userDetails[0].username;
      req.session.admin = userDetails[0].admin;
      res.sendFile("/views/index.html", { root: __dirname });
    } else {
      res
        .status(500)
        .send("You need to activate your account ! Please check your mail");
    }
  } else {
    res.status(500).send("Bad username / password!");
  }
});

route.get("/admin", function(req, res) {
  res.sendFile("/views/admin.html", { root: __dirname });
});

route.get("/newarticle", function(req, res) {
  res.sendFile("/views/new_article.html", { root: __dirname });
});

route.post("/newArticle", function(req, res) {
  console.log("Post on new article");
  if (req.body.action === "save") {
    db.newArticle({
      title: req.body.articleTitle,
      content: req.body.content,
      date: new Date(),
      isPublished: false
    });
    res.sendFile("/views/admin.html", { root: __dirname });
  } else {
    db.newArticle({
      title: req.body.articleTitle,
      content: req.body.content,
      date: new Date(),
      isPublished: true
    });
    res.sendFile("/views/admin.html", { root: __dirname });
  }
});

route.post("/newComment", async (req, res) => {
  console.log("new comment");
  console.log(req.body.button);
  await db.newComment(req.session.username, req.body.content, req.body.button);
  res.sendFile("/views/index.html", { root: __dirname });
});

route.all("/logout", function(req, res) {
  req.session.destroy(function() {
    console.log("user logged out.");
  });
  res.sendFile("/views/index.html", { root: __dirname });
});

//=========================== API ==============================

route.all("/articles", async (req, res) => {
  var articles;
  articles = await db.getAllArticles();
  try {
    articles = JSON.parse(articles);
  } catch (e) {
    console.log(articles);
    console.error(e);
  }
  res.send(articles);
});

route.all("/comments", async (req, res) => {
  var comments;
  comments = await db.getAllComments();
  try {
    comments = JSON.parse(comments);
  } catch (e) {
    console.log(comments);
    console.error(e);
  }
  res.send(comments);
});

app.use(route);

db.connect();
//db.disconnect();
