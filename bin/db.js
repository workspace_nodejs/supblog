const mongoose = require("mongoose");
const mongodb = require("mongodb");
const crypto = require("crypto");
const ObjectId = require("mongodb").ObjectID;
const mail = require("./mail");
mongoose.Promise = global.Promise;

var Schema = mongoose.Schema;
var User;

const motdepasse = "motdepasse1";
const url =
  "mongodb+srv://supblog:" +
  motdepasse +
  "@cluster0-ho5gk.mongodb.net/test?retryWrites=true";

var userSchema = new Schema({
  username: String,
  password: String,
  mail: String,
  active: Boolean,
  admin: Boolean
});

var articleSchema = new Schema({
  title: String,
  content: String,
  date: Date,
  isPublished: Boolean
});

var commentSchema = new Schema({
  author: String,
  body: String,
  parentArticle: String,
  date: Date
});

userSchema.methods.toString = function() {
  console.log(
    "id : " +
      this._id +
      " username : " +
      this.username +
      " password : " +
      this.password +
      " mail : " +
      this.mail +
      " isAdmin : " +
      this.admin
  );
};

articleSchema.methods.toString = function() {
  console.log(
    "title : " +
      this.title +
      "content : " +
      this.content +
      "date : " +
      this.Date +
      "is published ? :" +
      this.isPublished
  );
};

commentSchema.methods.toString = function() {
  console.log(
    "author : " +
      this.author +
      "body : " +
      this.body +
      "date : " +
      this.Date +
      "parent article  : " +
      this.parentArticle
  );
};

exports.connect = function() {
  mongoose
    .connect(url, { useNewUrlParser: true })
    .then(() => console.log("Connection succesful"))
    .catch(err => console.error(err));

  User = mongoose.model("User", userSchema);
  Article = mongoose.model("Article", articleSchema);
  Comment = mongoose.model("Comment", commentSchema);
};

exports.getAllUsers = function() {
  User.find(function(err, users) {
    if (err) {
      console.error(err);
    }
    console.log(users);
  });
};

exports.getUserByUsername = async function(username) {
  var user;
  await User.find({ username: username }, "_id username active admin", function(
    err,
    users
  ) {
    if (err) return handleError(err);
    user = JSON.stringify(users);
  });
  return user;
};

exports.isUnique = async function(username) {
  var number;
  await User.countDocuments({ username: username }).then(count => {
    number = count;
  });
  return number;
};

exports.exists = async function(username, password) {
  var number;
  var passHash = crypto.createHmac("sha256", password).digest("hex");
  await User.countDocuments({ username: username, password: passHash }).then(
    count => {
      number = count;
    }
  );
  console.log(number);
  return number;
};

exports.newUser = function(req) {
  var passHash = crypto.createHmac("sha256", req.password).digest("hex");

  var newUser = new User({
    username: req.username,
    password: passHash,
    mail: req.mail,
    active: false,
    admin: req.isadmin
  });

  newUser
    .save()
    .then(() => console.log("User added"))
    .catch(err => console.error(err));

  newUser.toString();

  mail.sendNewUserMail({
    username: req.username,
    mail: req.mail,
    id: newUser._id
  });
};

exports.activateUser = function(id) {
  User.updateOne({ _id: ObjectId(id) }, { active: true }, function(err, raw) {
    if (err) console.error(err);
    console.log("User update");
  });
};

exports.newArticle = function(req) {
  var newArticle = new Article({
    title: req.title,
    content: req.content,
    date: new Date(),
    isPublished: false
  });

  newArticle
    .save()
    .then(() => console.log("new article created"))
    .catch(err => console.log(err));

  newArticle.toString();
};

exports.getAllArticles = async function() {
  var article;
  await Article.find({ isPublished: true }, function(err, articles) {
    if (err) {
      console.error(err);
    }
    article = JSON.stringify(articles);
  });
  return article;
};

exports.getAllComments = async function() {
  var comment;
  await Comment.find(function(err, comments) {
    if (err) {
      console.error(err);
    }
    comment = JSON.stringify(comments);
  });
  return comment;
};

exports.newComment = async function(username, content, article) {
  var newComment = new Comment({
    author: username,
    body: content,
    parentArticle: article,
    date: new Date()
  });

  newComment
    .save()
    .then(() => console.log("new comment posted"))
    .catch(err => console.log(err));

  newComment.toString();
};
