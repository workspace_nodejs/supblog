const nodemailer = require("nodemailer");

var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "paul.n.moreau@gmail.com",
    pass: "celMDPdpm19"
  }
});

exports.sendNewUserMail = function(params) {
  var mailOptions = {
    from: "SUPBLOG <paul.n.moreau@gmail.com>",
    to: params.mail,
    subject: "Activation de votre compte Supblog",
    text: "Supblog",
    html:
      "<b>" +
      "Bonjour," +
      "</b></br></br>vous avez créé un compte pour " +
      params.username +
      " sur la plateforme supbank.</br>Merci de cliquer sur le lien si dessous pour l'activer.</br><a href=\"http://localhost:3000/activate/" +
      params.id +
      '">Valider</a></br>Cordialement'
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      return console.log(error);
    }
    console.log("[MAIL] - Message sent: " + info.response);
  });

  transporter.close();
};

exports.sendNewCommentMail = function(params) {
  var mailOptions = {
    from: "SUPBLOG <paul.n.moreau@gmail.com>",
    to: "m.roger.mathis@gmail.com",
    subject: "Nouveau commentaire Supblog",
    text: "Supblog",
    html:
      "<b>" +
      "Bonjour," +
      "</b></br></br>un nouveau commentaire à été publié sur l'article " +
      params.title +
      " </br>Cordialement"
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      return console.log(error);
    }
    console.log("Message sent: " + info.response);
  });

  transporter.close();
};
