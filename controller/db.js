const mongoose = require("mongoose");
const mongodb = require("mongodb");
const crypto = require("crypto");
const ObjectId = require("mongodb").ObjectID;
const mail = require("./mail");
mongoose.Promise = global.Promise;

var Schema = mongoose.Schema;
var User;

const motdepasse = "motdepasse1";
const url =
  "mongodb+srv://supblog:" +
  motdepasse +
  "@cluster0-ho5gk.mongodb.net/test?retryWrites=true";

//====================== SCHEMA ======================

var userSchema = new Schema({
  username: String,
  password: String,
  mail: String,
  active: Boolean,
  admin: Boolean
});

var articleSchema = new Schema({
  title: String,
  content: String,
  date: Date,
  isPublished: Boolean
});

var commentSchema = new Schema({
  author: String,
  body: String,
  parentArticle: String,
  date: Date
});

var likeSchema = new Schema({
  user: String,
  article: String
});

userSchema.methods.toString = function() {
  console.log(
    "id : " +
      this._id +
      " username : " +
      this.username +
      " password : " +
      this.password +
      " mail : " +
      this.mail +
      " isAdmin : " +
      this.admin
  );
};

articleSchema.methods.toString = function() {
  console.log(
    "title : " +
      this.title +
      " content : " +
      this.content +
      " date : " +
      this.Date +
      " is published ? :" +
      this.isPublished
  );
};

commentSchema.methods.toString = function() {
  console.log(
    "author : " +
      this.author +
      " body : " +
      this.body +
      " date : " +
      this.Date +
      " parent article  : " +
      this.parentArticle
  );
};

likeSchema.methods.toString = function() {
  console.log("user : " + this.user + " article : " + this.article);
};

//====================== CONN ======================

exports.connect = function() {
  mongoose
    .connect(url, { useNewUrlParser: true })
    .then(() => console.log("[DATABASE] - Connected to database"))
    .catch(err => console.error(err));

  User = mongoose.model("User", userSchema);
  Article = mongoose.model("Article", articleSchema);
  Comment = mongoose.model("Comment", commentSchema);
  Like = mongoose.model("Like", likeSchema);
};

//====================== NEW SCHEMA ======================

exports.newUser = function(req) {
  var passHash = crypto.createHmac("sha256", req.password).digest("hex");

  var newUser = new User({
    username: req.username,
    password: passHash,
    mail: req.mail,
    active: false,
    admin: req.isadmin
  });

  newUser
    .save()
    .then()
    .catch(err => console.error(err));

  //newUser.toString();

  mail.sendNewUserMail({
    username: req.username,
    mail: req.mail,
    id: newUser._id
  });
};

exports.newArticle = function(req) {
  var newArticle = new Article({
    title: req.title,
    content: req.content,
    date: new Date(),
    isPublished: req.isPublished
  });

  newArticle
    .save()
    .then(() => console.log("[ARTICLE] - New article"))
    .catch(err => console.log(err));

  //newArticle.toString();
};

exports.newComment = function(username, content, article) {
  var newComment = new Comment({
    author: username,
    body: content,
    parentArticle: article,
    date: new Date()
  });

  newComment
    .save()
    .then(() => console.log("[COMMENT] - New comment"))
    .catch(err => console.log(err));

  //newComment.toString();
};

exports.newLike = function(username, parentArticle) {
  var newLike = new Like({
    user: username,
    article: parentArticle
  });

  newLike
    .save()
    .then(() => console.log("[LIKE] - New like"))
    .catch(err => console.log(err));

  //newLike.toString();
};

//====================== USER QUERY ======================

exports.getAllUsers = function() {
  User.find(function(err, users) {
    if (err) {
      console.error(err);
    }
    console.log(users);
  });
};

exports.getUserByUsername = function(username) {
  var query = User.find({ username: username }, "_id username active admin");
  var promise = query.exec();
  return promise.then(function(users) {
    return JSON.stringify(users);
  });
};

exports.isUnique = function(username) {
  var query = User.countDocuments({ username: username });
  var promise = query.exec();
  return promise.then(function(count) {
    return count;
  });
};

exports.exists = function(username, password) {
  var passHash = crypto.createHmac("sha256", password).digest("hex");
  var query = User.countDocuments({ username: username, password: passHash });
  var promise = query.exec();
  return promise.then(function(count) {
    return count;
  });
};

exports.activateUser = function(id) {
  var query = User.updateOne({ _id: ObjectId(id) }, { active: true });
  var promise = query.exec();
  promise.then(function(doc) {
    console.log("[REGISTER] - Account activated");
  });
};

//====================== ARTICLE QUERY ======================

exports.getApiArticles = function(filter) {
  if (filter === "hidden") {
    var query = Article.find({ isPublished: false });
  } else if (filter === "published") {
    var query = Article.find({ isPublished: true });
  } else {
    var query = Article.find();
  }
  var promise = query.exec();
  return promise.then(function(articles) {
    return JSON.stringify(articles);
  });
};

exports.getApiArticle = function(id) {
  var query = Article.find({ _id: ObjectId(id) });
  var promise = query.exec();
  return promise.then(function(articles) {
    return JSON.stringify(articles);
  });
};

exports.publishArticle = function(id) {
  var query = Article.updateOne({ _id: ObjectId(id) }, { isPublished: true });
  var promise = query.exec();
  promise.then(function(doc) {
    console.log("[ARTICLE] - Article published");
  });
};

exports.updateArticle = function(params) {
  var query = Article.updateOne(
    { _id: ObjectId(params.id) },
    {
      title: params.title,
      content: params.content,
      date: new Date(),
      isPublished: params.isPublished
    }
  );
  var promise = query.exec();
  promise.then(function(doc) {
    console.log("[ARTICLE] - Article updated");
  });
};

//====================== COMMENTS QUERY ======================

exports.getAllComments = function() {
  var query = Comment.find();
  var promise = query.exec();
  return promise.then(function(comments) {
    return JSON.stringify(comments);
  });
};

//====================== LIKE QUERY ======================

exports.getAllLikes = function() {
  var query = Like.aggregate([
    {
      $group: {
        _id: "$article",
        count: { $sum: 1 }
      }
    }
  ]);
  var promise = query.exec();
  return promise.then(function(likes) {
    return JSON.stringify(likes);
  });
};
